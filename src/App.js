import metadata from "./metadata.json";
import { useState, useEffect } from "react";
import Field from "./components/Field";


function App() {
  const [elements, setElements] = useState(null);

  useEffect(() => {
    setElements(metadata);
    
  }, [])

  const { formName, formDesc, data } = elements ?? {};

  return (
    <div className="App container">
      <h3>{formName}</h3>
      <h5>{formDesc}</h5>
      <form>
        {data ? data.map((field, index) => <Field key={index} field={field} />) : null}

        <button type="submit" className="btn btn-primary">Submit</button>
      </form>
    </div>
  );
}

export default App;
