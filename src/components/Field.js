import React from 'react'
import Text from "./fields/Text";
import Select from "./fields/Select";
import Checkbox from "./fields/Checkbox";

const Field = ({ field: { fieldtype, fieldname, fieldlabel, fieldplaceholder, fieldrules, options } }) => {

  switch (fieldtype) {
    case "text":
      return (<Text
        fieldname={fieldname}
        fieldlabel={fieldlabel}
        fieldplaceholder={fieldplaceholder}
        fieldrules={fieldrules}
      />)

    case "select":
      return (<Select 
        fieldname={fieldname}
        fieldlabel={fieldlabel}
        fieldplaceholder={fieldplaceholder}
        fieldrules={fieldrules}
        options={options}
      />)

    case "checkbox":
      return (<Checkbox />)

    default:
      return null;
  }

}

export default Field
