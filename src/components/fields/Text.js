import React from 'react'

const Text = ({ fieldname, fieldlabel, fieldplaceholder, fieldrules }) => {
  return (
    <div className="mb-3">
      <label htmlFor={fieldname} className="form-label">{fieldlabel}</label>
      <input type="text" className="form-control" id={fieldname} placeholder={fieldplaceholder ? fieldplaceholder : ""} aria-describedby="emailHelp" />
      {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
    </div>
  )
}

export default Text
